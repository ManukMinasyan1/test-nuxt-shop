import products from "@/assets/products.json";

export const state = {
  products: []
};
export const getters = {
  PRODUCTS(state) {
    return state.products;
  }
};

export const mutations = {
  setStorageProducts(state) {
    localStorage.setItem("products", JSON.stringify(state.products));
  },
  initialiseProducts(state) {
    if (localStorage.getItem("products")) {
      state.products = JSON.parse(localStorage.getItem("products"));
    } else {
      localStorage.setItem("products", JSON.stringify(products));
      location.reload();
    }
  },
  store(state, payload) {
    let lastProduct = state.products[state.products.length - 1];
    let newProduct = JSON.parse(JSON.stringify(payload));
    newProduct.id = lastProduct.id + 1;
    state.products = [...state.products, newProduct];
    this.commit("setStorageProducts");
  },
  update(state, payload) {
    let index = state.products.findIndex(p => p.id === payload.id);
    state.products[index] = payload;
    this.commit("setStorageProducts");
  },
  destroy(state, id) {
    let index = state.products.findIndex(p => p.id === id);
    state.products.splice(index, 1);
    this.commit("setStorageProducts");
  }
};

export const actions = {
  initialiseProducts: ({ commit }) => {
    commit("initialiseProducts");
  },
  store: ({ commit }, payload) => {
    commit("store", payload);
  },
  update: ({ commit }, payload) => {
    commit("update", payload);
  },
  destroy: ({ commit }, id) => {
    commit("destroy", id);
  },
  getById: (_, id) => {
    let products = JSON.parse(localStorage.getItem("products"));
    return products.find(p => parseInt(p.id) === parseInt(id));
  }
};
