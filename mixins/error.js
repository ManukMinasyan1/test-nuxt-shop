export default {
  data() {
    return {
      errors: []
    };
  },
  methods: {
    getError(field) {
      return this.errors[field];
    }
  }
};
